#include "imagem.hpp"
#include "pgm.hpp"
#include <iostream>
#include <stdlib.h>

PGM::PGM(char *caminhoArq){
  this->readImage(caminhoArq);
}

PGM::~PGM(){

}

void PGM::setPixel(unsigned char *pixel){
  this->pixel = pixel;
}

unsigned char * PGM::getPixel(){
  return pixel;
}

int PGM::readImage(char *caminhoArq){

  int tamanhoArq;
  int largura, altura, maxCor;
  char *tipo, *charInicio, *tamMsg, *keyword;

  FILE *arq; 

  arq = fopen(caminhoArq, "rb");
  if (arq == NULL) {
    printf("Erro ao abrir o arquivo\n\n"); 
    return 0;
  }

  tipo = (char *) calloc(2, 1);
  charInicio = (char *) calloc(10, 1);
  tamMsg = (char *) calloc(3, 1);
  keyword = (char *) calloc(30, 1);
    
  fscanf(arq, "%s\n%s %s %s %d %d %d", tipo, charInicio, tamMsg, keyword, &largura, &altura, &maxCor); 
  printf("%s %s %s %s %d %d %d\n",tipo, charInicio, tamMsg, keyword, largura, altura, maxCor );
 
  if(strcmp(tipo,"P5")) {
    printf("Formato incorreto\n"); 
    return 0; 
  }

  setLargura(largura);
  setAltura(altura);
  setMaxCor(maxCor);
    
  fgetc(arq);
    tamanhoArq=(largura)*(altura);
    (this->pixel) = (unsigned char *) calloc(tamanhoArq, 1);
    if ((this->pixel) != NULL)
      fread( (this->pixel), tamanhoArq, 1, arq );
    fclose(arq);

  this->writeImage(tipo,getLargura(),getAltura(),getMaxCor(),this->pixel);  
  free(this->pixel);

  return 1;
}

int PGM::writeImage(char tipo[],int largura, int altura,int maxCor, unsigned char *pixel){
  FILE *newArq;
  newArq = fopen("novoaqr.ppm", "wb");
  if (newArq == NULL)
    return 0;
  if (fprintf(newArq, "%s\n%d %d\n%d\n",tipo ,largura, altura, maxCor) <= 0){
    fclose(newArq);
    return 0;
  }
  if (fwrite(pixel, largura*altura, 1, newArq) != 1){
    fclose(newArq);
    return 0;
  }
  fclose(newArq);

return 1;
}

