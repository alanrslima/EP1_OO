#include "imagem.hpp"
#include "ppm.hpp"
#include <iostream>
#include <stdlib.h>

PPM::PPM(char *caminhoArq){
  this->readImage(caminhoArq);
}

PPM::~PPM(){

}

void PPM::setRGB(unsigned char *rgb){
  this->rgb = rgb;
}

unsigned char * PPM::getRGB(){
  return rgb;
}

int PPM::readImage(char *caminhoArq){

  int tamanhoArq;
  int largura, altura, maxCor;
  char *tipo, *charInicio, *tamMsg, *keyword;

  FILE *arq; 

  arq = fopen(caminhoArq, "rb");
  if (arq == NULL) {
    printf("Erro ao abrir o arquivo"); 
    return 0;
  }

  tipo = (char *) calloc(2, 1);
  charInicio = (char *) calloc(10, 1);
  tamMsg = (char *) calloc(3, 1);
  keyword = (char *) calloc(30, 1);
    
  fscanf(arq, "%s\n%s %s %s %d %d %d", tipo, charInicio, tamMsg, keyword, &largura, &altura, &maxCor); 
 
  if(strcmp(tipo,"P6")) {
    printf("Formato incorreto\n"); 
    return 0; 
  }

  printf("Tipo:%s CharInicio:%s tamMsg:%s keyWord:%s largura:%d altura:%d maxCor:%d\n",tipo,charInicio,tamMsg,keyword,largura,altura, maxCor );

  setLargura(largura);
  setAltura(altura);
  setMaxCor(maxCor);
    
  fgetc(arq);
    tamanhoArq=3*(largura)*(altura);
    (this->rgb) = (unsigned char *) calloc(tamanhoArq, 1);
    if ((this->rgb) != NULL)
      fread( (this->rgb), tamanhoArq, 1, arq );
    fclose(arq);

  writeImage(tipo,largura,altura,maxCor,this->rgb);
  
  free(this->rgb);
  return 1;
}

int PPM::writeImage(char tipo[],int largura, int altura,int maxCor, unsigned char *RGB){

  FILE *newArq;
  newArq = fopen("novoaqr.ppm", "wb");
  if (newArq == NULL)
    return 0;
if (fprintf(newArq, "%s\n%d %d\n%d\n",tipo ,largura, altura, maxCor) <= 0){
    fclose(newArq);
    return 0;
  }
  if (fwrite(RGB, 3*largura*altura, 1, newArq) != 1){
    fclose(newArq);
    return 0;
  }
  fclose(newArq);

return 1;
}

