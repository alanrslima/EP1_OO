#include "imagem.hpp"
#include <iostream>
#include <stdlib.h>

Imagem::Imagem(){
}

Imagem::~Imagem(){

}

void Imagem::setLargura(int largura){
  this->largura = largura;
}
  
int Imagem::getLargura(){
  return largura;
}

void Imagem::setAltura(int altura){
  this->altura = altura;
}
  
int Imagem::getAltura(){
  return altura;
}

void Imagem::setMaxCor(int maxCor){
  this->maxCor = maxCor;
}
 
int Imagem::getMaxCor(){
  return maxCor;
}

/*
void Imagem::setTipo(char *tipo){
  this->tipo = tipo;
}

char * Imagem::getTipo(){
  return tipo;
}

void Imagem::setComentario(char *comentario){
  this->comentario = comentario;
}
  
char * Imagem::getComentario(){
  return comentario;
}
*/


