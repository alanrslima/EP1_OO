#include "imagem.hpp"
#include "ppm.hpp"
#include "pgm.hpp"
#include <iostream>

using namespace std;

int doMenu();

int main(int argc, char ** argv){

	int formatoSelecionado;
	char caminhoArq[70];

	formatoSelecionado = doMenu();

		if (formatoSelecionado == 1) {
			printf("Digite o caminho do arquivo PPM: ");
			scanf("%s", caminhoArq);
			PPM *ppm = new PPM(caminhoArq);
			delete ppm;
		}else if (formatoSelecionado == 2){
			printf("Digite o caminho do arquivo PGM: ");
			scanf("%s", caminhoArq);
			PGM *pgm = new PGM(caminhoArq);
			delete pgm;
		}

	return 0;

}

int doMenu(){

	int formatoSelecionado;

	printf("---------------------------------------------------------------------------\n" );
	printf("               Decifrador de mensagens em arquivos PPM/PGM\n");
	printf("---------------------------------------------------------------------------\n\n" );
	printf("[1] - Formato PPM\n");
	printf("[2] - Formato PGM\n\n");
	printf("Digite o indice correspondente ao formato que será utilizado: ");
	scanf("%d", &formatoSelecionado);
	if (formatoSelecionado == 1){
		return 1;
	}else if (formatoSelecionado == 2){
		return 2;
	} else{
		printf("Entrada inválida!\n\n");
		return -1;
	}
}
