#ifndef PGM_HPP
#define PGM_HPP

#include <iostream>
using namespace std;

class PGM: public Imagem{
private:
	unsigned char *pixel;
public:
	PGM(char *caminhoArq);
	~PGM();

	void setPixel(unsigned char *pixel);
	unsigned char * getPixel();

	int readImage(char *caminhoArq);	
	int writeImage(char tipo[],int largura, int altura,int maxCor, unsigned char *pixel);

};

#endif