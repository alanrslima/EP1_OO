#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
using namespace std;

class Imagem{
private:
  int largura;
  int altura; 
  int maxCor;
  /*
  char *tipo;
  char *inicialChar;
  char *tamanhoMsg;
  char *keyWord;
  */
public:
	Imagem();
	~Imagem();

	void setLargura(int largura);
	int getLargura();

	void setAltura(int altura);
	int getAltura();

	void setMaxCor(int maxCor);
	int getMaxCor();

/*
	void setTipo(char *tipo);
	char * getTipo();

	void setInicialChar(char *inicialChar);
	char * getInicialChar();

	void setTamanhoMsg(char *tamanhoMsg);
	char * getTamnhoMsg();

	void setKeyWord(char *KeyWord);
	char * getKeyWord();
*/

};

#endif