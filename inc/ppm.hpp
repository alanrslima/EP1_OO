#ifndef PPM_HPP
#define PPM_HPP

#include <iostream>
using namespace std;

class PPM: public Imagem{
private:
	unsigned char *rgb;
public:
	PPM(char *caminhoArq);
	~PPM();

	void setRGB(unsigned char *rgb);
	unsigned char * getRGB();

	int readImage(char *caminhoArq);	
	int writeImage(char tipo[],int largura, int altura,int maxCor, unsigned char *RGB);

};

#endif